package com.example.l10;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.ListIterator;

public class MainActivity extends AppCompatActivity {

    WebView web;
    Context context = null;
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        web = findViewById(R.id.webView);
        web.setWebViewClient(new WebViewClient());
        web.getSettings().setJavaScriptEnabled(true);

        final ArrayList<String> history = new ArrayList<>();
        final ArrayList<String> forwards = new ArrayList<>();
        final EditText urlET = (EditText) findViewById(R.id.urlET);

//Tehtävä 2
        final Button refreshButton = (Button) findViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (history.isEmpty())
                    toast("Nothing to refresh");
                else {
                    String url = history.get(history.size() - 1);
                    web.loadUrl(url);
                    toast("Refreshed");
                }
            }
        });
//Tehtävä 3
        final Button shoutButton = (Button) findViewById(R.id.shoutButton);
        shoutButton.setVisibility(View.GONE);
        shoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                web.evaluateJavascript("javascript:shoutOut()", null);
            }
        });
        final Button reverseButton = (Button) findViewById(R.id.reverseButton);
        reverseButton.setVisibility(View.GONE);
        reverseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                web.evaluateJavascript("javascript:initialize()", null);
            }
        });
//Tehtävä 4
        final Button backButton = (Button) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (history.size() >= 2) {
                    final ListIterator<String> hliter = history.listIterator(history.size());
                    String url = hliter.previous();
                    forwards.add(url);
                    hliter.remove();
                    url = hliter.previous();
                    web.loadUrl(url);
                } else
                    toast("No previous");
            }
        });
        final Button forwardButton = (Button) findViewById(R.id.forwardButton);
        forwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (forwards.size() >= 1) {
                    final ListIterator<String> fliter = forwards.listIterator(forwards.size());
                    String url = fliter.previous();
                    web.loadUrl(url);
                    history.add(url);
                    fliter.remove();
                } else
                    toast("No next");
            }
        });
//Tehtävä 1
        final Button searchButton = (Button) findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = urlET.getText().toString();
                if (url.isEmpty() || url.equals(" ")) {
                    toast("Give URL");
                } else {
                    if (url.equals("index.html")) {
                        url = "file:///android_asset/index.html";
                        backButton.setVisibility(View.GONE);
                        forwardButton.setVisibility(View.GONE);
                        shoutButton.setVisibility(View.VISIBLE);
                        reverseButton.setVisibility(View.VISIBLE);
                    }
                    else {
                        if (!url.contains("http://") && !url.contains("https://"))
                            url = "https://" + url;
                        if (!url.contains(".fi") && !url.contains(".com"))
                            url = url + ".fi";
                        backButton.setVisibility(View.VISIBLE);
                        forwardButton.setVisibility(View.VISIBLE);
                        shoutButton.setVisibility(View.GONE);
                        reverseButton.setVisibility(View.GONE);
                    }
                    web.loadUrl(url);
                    history.add(url);

//Tehtävä 5
                    if (!forwards.isEmpty()) {
                        for (int i = 0; i <= forwards.size(); i++) {
                            forwards.remove(0);
                        }
                    }
                }
            }
        });
    }
    public void toast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
