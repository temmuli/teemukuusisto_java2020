package com.example.l7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class MainActivity extends AppCompatActivity {

    TextView textV;
    EditText inputText;
    TextView outputText;
    EditText inputText2;
    TextView outputText2;
    EditText fileNameET, textET;
    Button saveButton, loadButton;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        textV = (TextView) findViewById(R.id.textView);
        inputText = (EditText) findViewById(R.id.editText);
        outputText = (TextView) findViewById(R.id.textView2);
        inputText2 = (EditText) findViewById(R.id.editText2);
        outputText2 = (TextView) findViewById(R.id.textView3);
        fileNameET = (EditText) findViewById(R.id.editText3);
        textET = (EditText) findViewById(R.id.editText4);
        saveButton = (Button) findViewById(R.id.saveButton);
        loadButton = (Button) findViewById(R.id.loadButton);

//Tehtävä 4
        inputText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                outputText2.setText(s);
            }
            @Override
            public void afterTextChanged(Editable s) {}
        } );
    }
//Tehtävä 1
    public void printing(View v) {
        System.out.println("Hello World!");
    }
//Tehtävä 2
    public void changing(View v) {
        textV.setText("Hello World!");
    }
//Tehtävä 3
    public void outputting(View v) {
        String inText = inputText.getText().toString();
        outputText.setText(inText);
    }
//Tehtävä 5
    public void saving(View v) {
        String fileName = fileNameET.getText().toString();
        String message = textET.getText().toString();
        try{
            OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_PRIVATE));
            ows.write(message);
            ows.close();
            showToast("Saved");
		}
		catch(IOException ex) {
		    Log.e("Exception", "Error in saving");
            showToast("Error");
		}
    }
    public void loading(View v) {
        String fileName = fileNameET.getText().toString();
        try {
            InputStream in = context.openFileInput(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String s = "";
            if ((s = br.readLine()) != null)
                textET.setText(s);
            else
                showToast("File is empty");
        }
		catch(IOException ex){
                Log.e("Exception", "Error in loading");
                showToast("Error");
            }
    }
    private void showToast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}