package com.example.l11;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    private TextView textView, displayTV;
    private EditText inputET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);
        textView.setVisibility(View.VISIBLE);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        inputET = (EditText) findViewById(R.id.inputET);
        displayTV = (TextView) findViewById(R.id.displayTV);

        nv = (NavigationView) findViewById(R.id.nv);
        dl = (DrawerLayout) findViewById(R.id.activity_main);
        t = new ActionBarDrawerToggle(this, dl, R.string.open, R.string.close);

        dl.addDrawerListener(t);
        t.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String[] sizeOptions = {"8", "12", "16", "20", "24"};
        Spinner sizeSpinner = (Spinner) nv.getMenu().findItem(R.id.sizeSpinner).getActionView();
        ArrayAdapter<String> sizeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, sizeOptions);
        sizeSpinner.setAdapter(sizeAdapter);
        sizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                textView.setTextSize(Integer.valueOf(parent.getItemAtPosition(position).toString()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        String[] colorOptions = {"Black", "Red", "White"};
        Spinner colorSpinner = (Spinner) nv.getMenu().findItem(R.id.colorSpinner).getActionView();
        ArrayAdapter<String> colorAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, colorOptions);
        colorSpinner.setAdapter(colorAdapter);
        colorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (parent.getItemAtPosition(position).toString()) {
                    case "Black":
                        textView.setTextColor(Color.parseColor("#000000"));
                        break;
                    case "Red":
                        textView.setTextColor(Color.parseColor("#EC1010"));
                        break;
                    case "White":
                        textView.setTextColor(Color.parseColor("#FFFFFF"));
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        String[] backgroundOptions = {"White", "Red", "Black"};
        Spinner backgroundSpinner = (Spinner) nv.getMenu().findItem(R.id.backgroundSpinner).getActionView();
        ArrayAdapter<String> backgroundAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, backgroundOptions);
        backgroundSpinner.setAdapter(backgroundAdapter);
        backgroundSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (parent.getItemAtPosition(position).toString()) {
                    case "Black":
                        textView.setBackgroundColor(Color.parseColor("#000000"));
                        break;
                    case "Red":
                        textView.setBackgroundColor(Color.parseColor("#EC1010"));
                        break;
                    case "White":
                        textView.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        String[] languageOptions = {"Suomi", "Englanti", "Ruotsi"};
        Spinner languageSpinner = (Spinner) nv.getMenu().findItem(R.id.languageSpinner).getActionView();
        ArrayAdapter<String> languageAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, languageOptions);
        languageSpinner.setAdapter(languageAdapter);
        languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (parent.getItemAtPosition(position).toString()) {
                    case "Suomi":
                        //Käännä suomeksi
                        break;
                    case "Englanti":
                        //Käännä englanniksi
                        break;
                    case "Ruotsi":
                        //Käännä ruotsiksi
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.visibility:
                        changeVisibility();
                        break;
                    case R.id.permission:
                        changePermission();
                        break;
                    case R.id.displayButton:
                        loadActivity();
                        break;
                    default:
                        return true;
                }
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (t.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    public void changeVisibility() {
        if (textView.getVisibility() == View.VISIBLE) {
            textView.setVisibility(View.GONE);
            nv.getMenu().findItem(R.id.visibility).setTitle("Set visible");
        } else if (textView.getVisibility() == View.GONE) {
            textView.setVisibility(View.VISIBLE);
            nv.getMenu().findItem(R.id.visibility).setTitle("Set invisible");
        }
    }

    public void changePermission() {
        if (nv.getMenu().findItem(R.id.permission).getTitle().toString().equals("Start editing")) {
            inputET.setEnabled(true);
            nv.getMenu().findItem(R.id.permission).setTitle("Stop editing");
        } else if (nv.getMenu().findItem(R.id.permission).getTitle().toString().equals("Stop editing")) {
            inputET.setEnabled(false);
            nv.getMenu().findItem(R.id.permission).setTitle("Start editing");
            textView.setText(inputET.getText().toString());
            textView.setText(getResources().getString(R.string.write_display_text_here));
        }
    }

    public void loadActivity() {
        Intent intent = new Intent(MainActivity.this, DisplayActivity.class);
        intent.putExtra("key", "nothing");
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String displayText = data.getStringExtra("key");
                displayTV.setText(displayText);
            }
        }
    }
}

