package com.example.l11;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class DisplayActivity extends AppCompatActivity {

    EditText displayET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        final EditText displayET = (EditText) findViewById(R.id.displayET);

        final Button backButton = (Button) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = displayET.getText().toString();
                Intent intent = new Intent();
                intent.putExtra("key", text);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        String text = displayET.getText().toString();
        Intent intent = new Intent();
        intent.putExtra("key", text);
        setResult(RESULT_OK, intent);
        finish();
    }
}
