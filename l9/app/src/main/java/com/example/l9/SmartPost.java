package com.example.l9;

public class SmartPost {
    String place_id, name, city, address, country, postalcode, availability;

    public SmartPost(String place_id, String name, String city, String address, String country, String postalcode, String availability) {
        this.place_id = place_id;
        this.name = name;
        this.city = city;
        this. address = address;
        this.country = country;
        this.postalcode = postalcode;
        this.availability = availability;
    }
}
