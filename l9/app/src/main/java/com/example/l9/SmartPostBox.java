package com.example.l9;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SmartPostBox {
    private SmartPostBox() {}
    private static SmartPostBox box = new SmartPostBox();
    public static SmartPostBox getInstance() {
        return box;
    }
    static ArrayList<SmartPost> postList = new ArrayList<>();

    public static void newPost(String place_id, String name, String city, String address, String country, String postalcode, String availability) {
        if (country.equals("EE")) {
            availability = availability.replaceAll("E", "ma");
            availability = availability.replaceAll("T", "ti");
            availability = availability.replaceAll("K", "ke");
            availability = availability.replaceAll("N", "to");
            availability = availability.replaceAll("R", "pe");
            availability = availability.replaceAll("L", "la");
            availability = availability.replaceAll("P", "su");

            country = "Viro";
        }
        else if (country.equals("FI"))
            country = "Suomi";
        SmartPost temp = new SmartPost(place_id, name, city, address, country, postalcode, availability);
        postList.add(temp);
    }
    public static ArrayList<String> getOptions() {
        ArrayList<String> postOptions = new ArrayList<>();
        postOptions.add("SmartPosts");
        for (SmartPost sp : postList) {
            postOptions.add(sp.city+" "+sp.name);
        }
        return postOptions;
    }
    public static SmartPost getPostInfo(String city_name) {
        SmartPost temp = null;
        String[] cityName = city_name.split(" ", 2);
        for (SmartPost sp : postList) {
            if ((sp.city.equals(cityName[0])) && (sp.name.equals(cityName[1]))) {
                temp = sp;
                break;
            } else {
                temp = null;
            }
        }
        return temp;
    }
    public static boolean checkLocation(String location) {
        for (SmartPost sp : postList) {
            if (sp.country.toLowerCase().equals(location) || sp.city.toLowerCase().equals(location))
                return true;
        }
        return false;
    }

    public static ArrayList<String> changeFilter(String location, String days, String after, String before) {
        ArrayList<String> filterList = new ArrayList<>();
        filterList.add("SmartPosts");
        String times = "";
        if (location.isEmpty() && days.isEmpty() && after.isEmpty() && before.isEmpty()) {
            location = "kaikki";
            LocalDateTime today = LocalDateTime.now(ZoneId.of("Europe/Helsinki"));
            days = today.format(DateTimeFormatter.ofPattern("EEEE"));
            times = today.format(DateTimeFormatter.ofPattern("HH"));
            times = times+"-"+(Integer.valueOf(times)+1);
        } else {
            if (location.isEmpty())
                location = "kaikki";
            days = dayFormatter(days);
            times = searchTimeFormatter(after, before);
        }

        for (SmartPost sp : postList) {
            if (sp.country.toLowerCase().equals(location.toLowerCase()) || sp.city.toLowerCase().equals(location.toLowerCase()) || location.equals("kaikki")) {
                if (sp.availability.contains(",")) {
                    String openings = sp.availability;
                    String[] dayData = openings.split(",");
                    for (int i = 0; i < dayData.length; i++) {
                        dayData[i] = dayData[i].trim();
                        List<String> dayTime = new ArrayList<> (Arrays.asList(dayData[i].split(" ", 2)));
                        if (dayTime.size() == 1)
                            dayTime.add("00-23");
                        String day = dayFormatter(dayTime.get(0));
                        if (day.contains(days) || days.equals("kaikki")) {
                            String time = timeFormatter(dayTime.get(1));
                            if (time.contains(times) || times.equals("kaikki")) {
                                filterList.add(sp.city + " " + sp.name);
                                break;
                            }
                        }
                    }
                } else if (sp.availability.equals("24h")) {
                    filterList.add(sp.city + " " + sp.name);
                } else {
                    String openings = sp.availability;
                    List<String> dayTime = new ArrayList<> (Arrays.asList(openings.split(" ", 2)));
                    if (dayTime.size() == 1)
                        dayTime.add("00-23");
                    String day = dayFormatter(dayTime.get(0));
                    if (day.contains(days) || days.equals("kaikki")) {
                        String time = timeFormatter(dayTime.get(1));
                        if (time.contains(times) || times.equals("kaikki")) {
                            filterList.add(sp.city + " " + sp.name);
                        }
                    }
                }
            } // if loppu
        } //for loppu
    return filterList;
    }

    public static String dayFormatter(String days) {
        int a = 0, b = 0;
        if (days.contains("-")) {
            String[] weekdays = {"ma", "ti", "ke", "to", "pe", "la", "su"};
            String[] search = days.split("-");
            for (int i = 0; i < weekdays.length; i++) {
                if (weekdays[i].equals(search[0])) {
                    a = i;
                }
                if (weekdays[i].equals(search[1])) {
                    b = i;
                }
            }
            days = "";
            for (int x = a; x < b+1; x++) {
                if (days.isEmpty())
                    days = weekdays[x];
                else
                    days = days+","+weekdays[x];
            }
        } else {
            if (days.equals("maanantai"))
                days = "ma";
            else if (days.equals("tiistai"))
                days = "ti";
            else if (days.equals("keskiviikko"))
                days = "ke";
            else if (days.equals("torstai"))
                days = "to";
            else if (days.equals("perjantai"))
                days = "pe";
            else if (days.equals("lauantai"))
                days = "la";
            else if (days.equals("sunnuntai"))
                days = "su";
            else if (days.isEmpty())
                days = "kaikki";
            else if (days.contains("24h"))
                days = "24h";
        }
        return days;
    }
    public static String searchTimeFormatter(String after, String before) {
        int a = 0, b = 0;
        if (after.isEmpty() && before.isEmpty())
            return "kaikki";
        if (after.isEmpty())
            after = "00";
        else if (after.length() == 1)
            after = "0"+after;

        if (before.isEmpty())
            before = "23";
        else if (before.length() == 1)
            before = "0"+before;

            String[] hours = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
            for (int i = 0; i < hours.length; i++) {
                if (hours[i].equals(after)) {
                    a = i;
                }
                if (hours[i].equals(before)) {
                    b = i;
                }
            }
            String time = "";
            for (int x = a; x <= b; x++) {
                if (time.isEmpty())
                    time = hours[x];
                else
                    time = time + "," + hours[x];
            }
        return time;
    }
     public static String timeFormatter(String time) {
         int a = 0, b = 0;
        if (time.contains("-")) {
            time = time.replaceAll("\\.00", "");
            time = time.replaceAll(" ", "");
            String[] search = time.split("-");
            if (search[0].length() == 1)
                search[0] = "0"+search[0];
            if (search[1].length() == 1)
                search[1] = "0"+search[1];
            String[] hours = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
            for (int i = 0; i < hours.length; i++) {
                if (hours[i].equals(search[0])) {
                    a = i;
                }
                if (hours[i].equals(search[1])) {
                    b = i;
                }
            }
            time = "";
            for (int x = a; x <= b; x++) {
                if (time.isEmpty())
                    time = hours[x];
                else
                    time = time + "," + hours[x];
            }
        }
        return time;
     }
}
