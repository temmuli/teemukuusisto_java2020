package com.example.l9;

import androidx.appcompat.app.AppCompatActivity;
import org.w3c.dom.Element;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


public class MainActivity extends AppCompatActivity {

    Context context = null;
    EditText locationET, dayET, afterET, beforeET;
    Spinner postSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        final SmartPostBox box = SmartPostBox.getInstance();

        readXML("Suomi", "https://iseteenindus.smartpost.ee/api/?request=destinations&country=FI&type=APT");
        final String[] postOptions = readXML("Viro", "https://iseteenindus.smartpost.ee/api/?request=destinations&country=EE&type=APT");
        final TextView screenTV = (TextView) findViewById(R.id.screenTV);

        postSpinner = findViewById(R.id.postSpinner);
        ArrayAdapter<String> postAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, postOptions);
        postAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        postSpinner.setAdapter(postAdapter);
        postSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    String post = parent.getItemAtPosition(position).toString();
                    SmartPost sp = null;
                    sp = SmartPostBox.getPostInfo(post);
                    if (post.equals("SmartPosts"))
                        screenTV.setText(" ");
                    else if (sp != null) {
                        screenTV.setText("Nimi: " + sp.name + "\nOsoite: " + sp.address + "\nKaupunki: " + sp.city + "\nMaa: " + sp.country + "\nPostinumero: " + sp.postalcode + "\nAukioloajat: " + sp.availability);
                    } else if (sp == null)
                        screenTV.setText("SmartPostin: " + post + " tietoja ei löytynyt");
                } else {
                    String post = "kaikki";
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        locationET = (EditText) findViewById(R.id.locationET);
        locationET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    screenTV.setText(" ");
                    filtering();
                } else if (!SmartPostBox.checkLocation(s.toString().toLowerCase()))
                    screenTV.setText("Sijaintia ei löytynyt");
                else {
                    screenTV.setText(" ");
                    filtering();
                }

            }
        });

        dayET = (EditText) findViewById(R.id.dayET);
        dayET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 2 || s.toString().length() >= 5 || s.toString().toLowerCase().equals("24h")) {
                    screenTV.setText(" ");
                    filtering();
                } else if (s.toString().isEmpty()) {
                    screenTV.setText(" ");
                    filtering();
                } else {
                    screenTV.setText("Anna ajankohta muodossa pv tai pv-pv tai 24h");
                }
            }
        });

        afterET = (EditText) findViewById(R.id.afterET);
        afterET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().matches("^([0-1]?[0-9]|2[0-3])$")) {
                    screenTV.setText(" ");
                    filtering();
                } else if (s.toString().isEmpty()) {
                    screenTV.setText(" ");
                    filtering();
                } else
                    screenTV.setText("Anna kellonaika numerona 00-23");
            }
        });

        beforeET = (EditText) findViewById(R.id.beforeET);
        beforeET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().matches("^([0-1]?[0-9]|2[0-3])$")) {
                    screenTV.setText(" ");
                    filtering();
                } else if (s.toString().isEmpty()) {
                    screenTV.setText(" ");
                    filtering();
                } else
                    screenTV.setText("Anna kellonaika numerona 00-23");
            }
        });
        final Button nowButton = (Button) findViewById(R.id.nowButton);
        nowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> filterList = SmartPostBox.changeFilter("", "", "", "");
                postSpinner.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, filterList));
            }
        });
        final Button allButton = (Button) findViewById(R.id.allButton);
        allButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postSpinner.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, postOptions));
            }
        });
    }

    public void filtering() {
        String location = locationET.getText().toString().toLowerCase();
        String days = dayET.getText().toString().toLowerCase();
        String after = afterET.getText().toString();
        String before = beforeET.getText().toString();
        ArrayList<String> filterList = SmartPostBox.changeFilter(location, days, after, before);
        postSpinner.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, filterList));
    }

    public String[] readXML(String site, String url) {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(url);
            doc.getDocumentElement().normalize();
            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
            if (site.equals("Suomi")) {
                NodeList suomiList = doc.getDocumentElement().getElementsByTagName("item");
                for (int i = 0; i < suomiList.getLength(); i++) {
                    Node node = suomiList.item(i);

                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;

                        String place_id = element.getElementsByTagName("place_id").item(0).getTextContent();
                        String name = element.getElementsByTagName("name").item(0).getTextContent().replace("Posti Parcel Locker, ","");
                        String city = element.getElementsByTagName("city").item(0).getTextContent();
                        String address = element.getElementsByTagName("address").item(0).getTextContent();
                        String country = element.getElementsByTagName("country").item(0).getTextContent();
                        String postalcode = element.getElementsByTagName("postalcode").item(0).getTextContent();
                        String availability = element.getElementsByTagName("availability").item(0).getTextContent();
                        SmartPostBox.newPost(place_id, name, city, address, country, postalcode, availability);
                    }
                }
            } else if (site.equals("Viro")) {
                NodeList viroList = doc.getDocumentElement().getElementsByTagName("item");
                for (int i = 0; i < viroList.getLength(); i++) {
                    Node node = viroList.item(i);

                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;

                        String place_id = element.getElementsByTagName("place_id").item(0).getTextContent();
                        String name = element.getElementsByTagName("name").item(0).getTextContent();
                        String city = element.getElementsByTagName("city").item(0).getTextContent();
                        String address = element.getElementsByTagName("address").item(0).getTextContent();
                        String country = element.getElementsByTagName("country").item(0).getTextContent();
                        String postalcode = element.getElementsByTagName("postalcode").item(0).getTextContent();
                        String availability = element.getElementsByTagName("availability").item(0).getTextContent();
                        SmartPostBox.newPost(place_id, name, city, address, country, postalcode, availability);
                    }
                }
            }
            String[] postOptions = new String[SmartPostBox.getOptions().size()];
            SmartPostBox.getOptions().toArray(postOptions);
            return postOptions;

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        String[] failureOptions = {"SmartPosts"};
        return failureOptions;
    }
}