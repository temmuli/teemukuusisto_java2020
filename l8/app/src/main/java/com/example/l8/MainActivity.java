package com.example.l8;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
// import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    Context context = null;
    DecimalFormat formatter = new DecimalFormat("#0.00");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
//Tehtävä 1
        final BottleDispenser dispenser = BottleDispenser.getInstance();
        dispenser.addBottle("Pepsi Max", 0.5, 1.80);
        dispenser.addBottle("Pepsi Max", 1.5, 2.20);
        dispenser.addBottle("Coca-Cola Zero", 0.5, 2.00);
        dispenser.addBottle("Coca-Cola Zero", 1.5, 2.50);
        dispenser.addBottle("Fanta Zero", 0.5, 1.95);
        dispenser.addBottle("Pepsi Max", 0.5, 1.80);
        dispenser.addBottle("Pepsi Max", 1.0, 2.00);
        dispenser.addBottle("Coca-Cola Zero", 0.5, 2.00);
        dispenser.addBottle("Coca-Cola Zero", 1.0, 2.10);
        dispenser.addBottle("Fanta Zero", 1.0, 2.20);
        dispenser.addBottle("Fanta Zero", 1.5, 3.00);

        String[] brandOptions = {"Pepsi Max", "Coca-Cola Zero", "Fanta Zero"};
        final Spinner brandSpinner = findViewById(R.id.brandSpinner);
        ArrayAdapter<String> brandAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, brandOptions);
        brandAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        brandSpinner.setAdapter(brandAdapter);
        brandSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String name = parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        String[] sizeOptions = {"0.5", "1.0", "1.5"};
        final Spinner sizeSpinner = findViewById(R.id.sizeSpinner);
        ArrayAdapter<String> sizeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sizeOptions);
        sizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sizeSpinner.setAdapter(sizeAdapter);
        sizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                double size = Double.parseDouble(parent.getItemAtPosition(position).toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        final TextView screen = (TextView) findViewById(R.id.screen);
        final TextView moneyView = (TextView) findViewById(R.id.moneyView);
        final SeekBar moneyBar = (SeekBar) findViewById(R.id.moneyBar);
        final TextView seekView = (TextView) findViewById(R.id.seekView);

        moneyBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekView.setText(formatter.format(progress));
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        final Button addButton = findViewById(R.id.addButton);
		addButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
            BottleDispenser.addMoney(moneyBar.getProgress());
            screen.setText("Money added: "+moneyBar.getProgress());
            moneyView.setText("Money: " +formatter.format(BottleDispenser.money));
            moneyBar.setProgress(0);
            }
        });
        final Button buyButton = findViewById(R.id.buyButton);
        buyButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                String name = brandSpinner.getSelectedItem().toString();
                double size = Double.parseDouble(sizeSpinner.getSelectedItem().toString());
                int number = dispenser.buyBottle(name, size);
                if (number == 1)
                    screen.setText("Not enough money");
                else if (number == 2)
                    screen.setText(size+" "+name+" sold out");
                else if (number == 0)
                    screen.setText(size+" "+name+" came out");
                moneyView.setText("Money: " +formatter.format(BottleDispenser.money));
            }
        });
        final Button returnButton = findViewById(R.id.returnButton);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                double coins = 0;
                coins = BottleDispenser.returnMoney(BottleDispenser.money);
                screen.setText("Money returned:" +formatter.format(coins));
                moneyView.setText("Money: " +formatter.format(BottleDispenser.money));
            }
        });
        final String fileName = "receipts.txt";
        final Button receiptButton = findViewById(R.id.receiptButton);
        receiptButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                try{
                    Bottle temp = BottleDispenser.getLast();
                    @SuppressLint("SimpleDateFormat")
                    DateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                    Date date = new Date();
                    OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_PRIVATE));
                    ows.write("Receipt");
                    ows.write("\nDate & time: " +dateFormatter.format(date));
                    ows.write("\nProduct: "+temp.getSize()+" "+temp.getName());
                    ows.write("\nPrice: " +temp.getPrice() +"\n");
                    ows.close();
                    screen.setText("Receipt printed");
				}
				catch(IOException ex) {
                    Log.e("Exception", "Error in printing the receipt");
                    screen.setText("Error in printing the receipt");
                }
            }
        });
    }
 /*   private void showToast(String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    } */
}
