package com.example.l8;

import java.util.ArrayList;

public class BottleDispenser {
    private int bottles;
    private ArrayList<Bottle> bottleList = new ArrayList<>();
    private static ArrayList<Bottle> receiptList = new ArrayList<>();
    protected static double money = 0.0f;

//Tehtävä 1
    private BottleDispenser() {}
    private static BottleDispenser dispenser = new BottleDispenser();
    public static BottleDispenser getInstance() {
        return dispenser;
    }

    public static void addMoney(double amount){
        money += amount;
    }
    public void addBottle(String brand, double amount, double cost) {
        Bottle temp = new Bottle(brand, amount, cost);
        bottleList.add(temp);
        bottles++;
    }

    public int buyBottle(String name, double size) {
        int index = -1;
        for(Bottle x : bottleList) {
            if (name == x.getName() && size == x.getSize()){
                index = bottleList.indexOf(x);
                break;
            }
        }
        if (index == -1)
            return 2;
        if (money >= bottleList.get(index).getPrice()) {
            money -= bottleList.get(index).getPrice();
            if (!receiptList.isEmpty())
                receiptList.remove(0);
            receiptList.add(bottleList.get(index));
            bottleList.remove(index);
            bottles--;
            return 0;
        }
        else {
            return 1;
        }
    }
    public static double returnMoney(double coins) {
        if (coins <= 0) {
            money = 0;
            return money;
        }
        else {
            money = 0;
            return coins;
        }
    }
    public static Bottle getLast() {
        Bottle temp = receiptList.get(0);
        return temp;
    }
}
